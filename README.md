# Angularjs for novice homework

## Alex Barna

### Add new fields to form and table: 
-	[x]Deadline (date)
-	[x]Responsible (string)
-	[x]Est. Hours (number)
-	[ ]Other fields (optional)

### Add new functions:
-	[x]Remove task
-	[x]Remove all completed tasks. This button is shown when the 
list of tasks is not empty and there is at least one completed task.
-	[x]Edit task 
-   [x]Create and use new directives

### Create your own improvement of this task. Describe and implement it.
-	[x]Clear button for new item

### Optional:
-	[ ]Sort data in a table by clicking on a header of a column (asc/desc)
-	[x]Highlight rows in table if task is undone and deadline is missed.
-	[x]Use modal window for adding/editing items
-	[x]Add checkbox to the header of checkbox column. It should done/undone all tasks.