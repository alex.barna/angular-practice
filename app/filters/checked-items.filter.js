(function () {
    'use strict';

    angular.module('app')
        .filter('checkedItems', checkedItems);

    function checkedItems() {
        return function (items, showComplete) {
            let resArray = [];

            if (angular.isArray(items)) {
                angular.forEach(items, (item) => {
                    if (!item.done || showComplete) {
                        resArray.push(item);
                    }
                });
                return resArray;
            }
            else {
                return items;
            }
        };
    }

})();