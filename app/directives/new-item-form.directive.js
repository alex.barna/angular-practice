﻿(function () {
    'use strict';

    angular.module('app')
        .directive('newItemForm', newItemForm);

    function newItemForm() {
        return {
            restrict: 'E',
            templateUrl: 'directives/new-item-form.directive.html'
        };
    }

})();