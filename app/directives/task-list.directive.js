(function () {
    'use strict';

    angular.module('app')
        .directive('viTaskList', viTaskList);

    function viTaskList() {
        return {
            restrict: 'E',
            templateUrl: 'directives/task-list.directive.html'
        };
    }

})();