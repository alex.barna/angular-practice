(function () {
    'use strict';

    angular.module('app')
        .directive('editDialog', editDialog);

    function editDialog() {
        return {
            restrict: 'E',
            templateUrl: 'directives/edit-dialog.directive.html'
        };
    }

})();