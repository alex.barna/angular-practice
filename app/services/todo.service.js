(function () {
    'use strict';

    angular.module('app')
        .factory("todoService", todoService);

    todoService.$inject = ['$http', '$q', '$mdDialog', 'model'];
    function todoService($http, $q, $mdDialog, model) {
        let service = {
            addNewItem,
            clearItem,
            deleteItem,
            deleteAll,
            editItem,
            cancelEdit,
            saveEdit,
            getItems,
            incompleteCount,
            completeCount,
            warningLevel,
            deadlineMissed,
            checkAll
        };

        return service;

        function addNewItem(newItem) {
            if (newItem && newItem.action) {
				validateItem(newItem);
                model.items.push({
                    action: newItem.action,
                    done: false,
					responsible: newItem.responsible,
					estHours: newItem.estHours,
                    deadline: newItem.deadline
                });

                clearItem(newItem);
            }
        }

		function validateItem(item){
			item.responsible = item.responsible ? item.responsible : "Nobody";
			item.estHours = !isNaN(item.estHours) ? Number(item.estHours) : 0;
            item.deadline = item.deadline ? new Date(item.deadline) : new Date(Date.now());
		}
		
		function clearItem(item){
			item.action = '';
			item.responsible = '';
			item.estHours = '';
            item.deadline = '';
            angular.element(document.querySelector('#deadline')).val('');
		}

        function deleteItem(event, item){
            let confirm = $mdDialog.confirm()
                .title('Please confirm delete')
                .textContent('Would you like to delete action: "'+item.action+'" ?')
                .targetEvent(event)
                .ok('Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function() {
                model.items = model.items.filter(function(current){
                    return current !== item;
                });
                $mdDialog.show(
                  $mdDialog.alert()
                    .parent(angular.element(document.querySelector('body')))
                    .clickOutsideToClose(true)
                    .title('Delete was successful!')
                    .textContent('Successfully deleted item!')
                    .ok('Ok!')
                    .targetEvent(event)
                );
            }, function() {
                
            });
        }

        function deleteAll(event){
            let confirm = $mdDialog.confirm()
                .title('Please confirm delete')
                .textContent('Would you like to delete all completed action?')
                .targetEvent(event)
                .ok('Delete all')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function() {
                model.items = model.items.filter(function(current){
                    return !current.done;
                });
                $mdDialog.show(
                  $mdDialog.alert()
                    .parent(angular.element(document.querySelector('body')))
                    .clickOutsideToClose(true)
                    .title('Delete was successful!')
                    .textContent('Successfully deleted all completed item!')
                    .ok('Ok!')
                    .targetEvent(event)
                );
            }, function() {
                
            });
        }

        function editItem(event, $ctrl, item) {
            Object.assign($ctrl.editItem, item);
            validateItem($ctrl.editItem);
            $ctrl.editItem.originalAction = item.action;

            $mdDialog.show({
              contentElement: '#editItem',
              parent: angular.element(document.body),
              targetEvent: event
            });
        }

        function cancelEdit(){
            $mdDialog.hide();
        }

        function saveEdit(editItem){
            validateItem(editItem);
            angular.forEach(model.items, (item) => {
                if(item.action === editItem.originalAction){
                    item.action = editItem.action;
                    item.responsible = editItem.responsible;
                    item.estHours = editItem.estHours;
                    item.deadline = editItem.deadline;
                    return;
                }
            });
            $mdDialog.hide();
        }

        function getItems() {
            return $http
                .get('data/todo.json')
                .then(response => response.data)
                .catch(() => $q.reject('Error in getItems method'));
        }

        function incompleteCount() {
            let count = 0;

            angular.forEach(model.items, (item) => {
                if (!item.done) count++;
            });

            return count;
        }

        function completeCount() {
            return model.items && model.items.length-incompleteCount();
        }

        function warningLevel() {
            return incompleteCount(model.items) < 3
                ? 'label-success'
                : 'label-warning';
        }

        function deadlineMissed(item){
            return !item.done && new Date(item.deadline)<new Date(Date.now())
                ? 'danger'
                : '';
        }

        function checkAll(checked){
            angular.forEach(model.items, (item) => {
                item.done = checked;
            });
        }
    }

})();