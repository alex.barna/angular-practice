(function () {
    'use strict';

    angular.module('app')
        .controller('TodoController', TodoController);

    TodoController.$inject = ['model', 'todoService'];

    function TodoController(model, todoService) {
        let $ctrl = this;

        $ctrl.todo = model;
        Object.assign($ctrl, todoService);

        $ctrl.showComplete = true;
        $ctrl.allChecked = false;

        $ctrl.a = "Vitaliy";
        $ctrl.myHTML = "<span>Vitaliy</span>";
    }

})();